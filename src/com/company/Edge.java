package com.company;

import java.util.Objects;

public class Edge {

    private int weight;
    private double pheromone;
    private int startNode;
    private int endNode;

    public int getStartNode() {
        return startNode;
    }

    public void setStartNode(int startNode) {
        this.startNode = startNode;
    }

    public int getEndNode() {
        return endNode;
    }

    public void setEndNode(int endNode) {
        this.endNode = endNode;
    }

    public Edge(int weight) {
        this.weight = weight;
    }

    public Edge(int weight, double pheromone) {
        this.weight = weight;
        this.pheromone = pheromone;
    }

    public Edge(int weight, int startNode, int endNode) {
        this.weight = weight;
        this.startNode = startNode;
        this.endNode = endNode;
    }

    public Edge(int weight, double pheromone, int startNode, int endNode) {
        this.weight = weight;
        this.pheromone = pheromone;
        this.startNode = startNode;
        this.endNode = endNode;
    }

    public Edge(Edge ed) {
        this.weight = ed.weight;
        this.pheromone = ed.pheromone;
        this.startNode = ed.endNode;
        this.endNode = ed.startNode;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getPheromone() {
        return pheromone;
    }

    public void setPheromone(double pheromone) {
        this.pheromone = pheromone;
    }

    public void updatePheromone(double pheromone){
        this.pheromone = this.pheromone + pheromone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge edge = (Edge) o;
        return weight == edge.weight &&
                Double.compare(edge.pheromone, pheromone) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(weight, pheromone);
    }
}



//1 2 3 4 5
//2 3 4 5 - > idziemy np do 2
//z 2 wybieramy sposrod 3 4 5 ? -> idziemy do np 4
//z 4 wybieramy sposrod 3 5 ? -> idziemy do np. 3
//z 3 wybieramy 5 ? -> idziemy do 5
//z 5 idziemy do startu 1