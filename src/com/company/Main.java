package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        System.out.println("Podaj liczbę wierzchołków \n");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        System.out.println("Podaj maksymalna wagę krawedzi \n");
        int maxWaga = sc.nextInt();
        Graph graf = new Graph(new Edge[n][n]);
        graf.createRandomGraph(maxWaga, true);
        graf.readEdgeWeight();

        do {
            System.out.println("Podaj liczbe mrowek \n");
            int ants = sc.nextInt();
            String factorString;
            double factor = 0;
            BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
            do {
                System.out.println("Podaj wspolczynnik parowania feromonu od 0 do 1 (np. 0.035, 0.3, 0.55 itd.). Brak parowania wstaw 0!");
                try {
                    factorString = input.readLine();
                    factor = Double.parseDouble(factorString);
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }

            } while (factor > 1 && factor <= 0);




            int j = 0;
            //Algorithm start
            //Creating i - ants
            Ant[] ant = new Ant[ants];
            Ant tmpBestAnt = new Ant(new ArrayList<>());
            for (int i = 0; i < ants; i++) {
                ant[i] = new Ant(new ArrayList<>());
            }
            do {
                //Path and weight for i - ant
                for (int i = 0; i < ants; i++) {
                    ant[i].setPath(graf.findRouteForAnt(1, 2.0));

                    Iterator<Edge> listIterator = ant[i].getPath().iterator();
                    while (listIterator.hasNext()) {
                        Edge nextElement = listIterator.next();
                        ant[i].addWeight(nextElement.getWeight());
//                System.out.print(nextElement.getWeight() + " ");
//                System.out.print(nextElement.getPheromone());
                        //         System.out.print(nextElement.getStartNode()/* + "|  Wezel koncowy: " + nextElement.getEndNode() + " ||"*/);
                    }
//                    System.out.println("\n Waga dla mrówki" + i + ": " + ant[i].getPathWeight() + "\n");
                    if (ant[i].getPathWeight() < tmpBestAnt.getPathWeight() || tmpBestAnt.getPathWeight() == 0)
                        tmpBestAnt = new Ant(ant[i]);
                }

                graf.evaporation(factor);
                //Update pheromone on path
                for (int i = 0; i < ants; i++) {
                    Iterator<Edge> listIterator = ant[i].getPath().iterator();
                    while (listIterator.hasNext()) {
                        Edge nextElement = listIterator.next();
                        nextElement.updatePheromone(1 / Double.valueOf(ant[i].getPathWeight()));
                    }
                }

                //clear path and other data in current iteration
                for (int i = 0; i < ants; i++) {
                    if (j == 99) {
                        break;
                    }
                    ant[i].clearPath();
                }

                j++;
            } while (j < 100);

//            graf.readEdgePheromone();
//            HashSet<Integer> weight = new HashSet();
//        for (int i = 0; i < ants; i++) {
//            weight.add(ant[i].getPathWeight());
//            System.out.print(ant[i].getPathWeight());
//        }
//
//        System.out.println("Znaleziono " + weight.size() +" rozwiazan.");
//        for(int i:weight){
//            System.out.println(i);
//        }

            System.out.println("Best path weight: " + tmpBestAnt.getPathWeight() + " |best path: ");
            Iterator<Edge> iterator = tmpBestAnt.getPath().iterator();
            while (iterator.hasNext()) {
                Edge nextElement = iterator.next();
                System.out.println(nextElement.getStartNode() + " -> " + nextElement.getEndNode());
            }

            graf.resetEdgePheromone();


        }while(true);
    }
}
